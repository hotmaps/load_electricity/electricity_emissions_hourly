[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687155.svg)](https://doi.org/10.5281/zenodo.4687155)

# CO2 emission factors for electricity



## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation

This datasets includes hourly average CO2 emission factor based on monthly electricity generation data for EU28 on country level (NUTS0). 

The ENTSO-E power statistics platform [1] provides the annual generation mix for each MS per energy carrier. The monthly electricity generation by source has been downloaded for all countries included in the Hotmaps toolbox (see [1]). 
From the monthly electricity generation data downloaded from ENTSO-E CO2 emission factors were derived for each country. For each energy carrier an emission factor and average conversion efficiency was assumed to derive CO2 emissions from monthly electricity generation per energy carrier.
Emission factors for energy carriers and conversion efficiencies of power plants for each energy carrier were assumed to be uniform over all countries.
For assumptions on emission factors and conversion efficiencies per energy carrier please see [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 2.8.1.2 page 136.

The emission factors will be used in the Hotmaps toolbox to estimate the environmental impact of electrical heating systems.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 2.8 page 135ff.

Please note that generation data after the year 2015 is available at [2]

### Limitations of the dataset

Data on the electricity generation mix is only provided on an annual basis. The generation mix of individual hours can deviate significantly from the annual average mix. It also has to be noted that import and export of electricity is not considered and the data only provides information on the generation mix within country boarders.
The same applies for the data provided on emission factors. Emission factors for individual hours can deviate significantly from monthly averages in particular when cross-border electricity flows are considered. The emission factors are also based on average conversion efficiencies and average emission factors for primary energy carriers. In reality, the conversion efficiencies for individual countries can deviate significantly depending on the age of the power plant fleet within each country and on the specific energy carriers used to generate electricity.





### References
[1] [ENTSO-E - European Network of Transmission System Operators for Electricity](https://www.entsoe.eu/db-query/production/monthly-production-for-all-countries)
[2] [ENTSO-E - Power statistics](https://www.entsoe.eu/data/power-stats/)


## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 


## Authors
Michael Hartner <sup>*</sup>,

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/)
Institute of Energy Systems and Electrical Drives
Gusshausstrasse 27-29/370
1040 Wien


## License

Copyright © 2016-2018: Michael Hartner
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.